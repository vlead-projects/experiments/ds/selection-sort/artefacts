#+TITLE:Demonstration artefact for selection Sort (Javascript)
#+AUTHOR:VLEAD
#+DATE: 10/01/2019
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Selection sort class
+ Creates Selection_sort class, which has all the variables
  required during its running
+ We use "let" to create an object of this class
+ iteratro1 -> Used to keep track while traversing the array
+ iterator2 -> Used to keep track while traversing the
  array(iterator1 + 1)
+ finished -> Determines whether or not the sort is finished
+ action -> Determines the current action being performed
+ fn_name -> Name of the function (Bubble sort here)
+ card -> Variable that corresponds to DOM elements that
  represent the array
+ comparisons -> Counts the number of comparisons
+ swaps -> Counts the number of swaps
+ operation -> Determined current operation
+ interval -> Varialble corresponding to setting and
  unsetting the slider time interval
+ minimum -> Variable storing the minimum number encountered
  in any of the iterations
+ num -> Variable that stores the array
+  Array has been initialized to contain all zero values to
  allocate the memory at the start itse
+ Creating an object of the Selection_sort using "let"
#+NAME: demo-universal-class
#+BEGIN_SRC js
class Selection_sort{
  constructor(){
    this.iterator1 = 0;
    this.iterator2 = 0;
    this.numOfCards = 12;
    this.finished = false;
    this.action = 0;
    this.fn_name = "";
    this.card;
    this.comparisons = 0;
    this.swaps = 0;
    this.operation = "";
    this.interval = 0;
    this.prev = -1;
    this.minimum = 0;
    this.num = [];
  };
};
let selection_artefact = new Selection_sort();

#+END_SRC

* Main Function
A function that performs the body onload functions
#+NAME: main-functions
#+BEGIN_SRC js
function main_functions() { 
  randomise();
  handlers();
}
document.body.onload = function() {main_functions();}

#+END_SRC

* Adds all event handlers
A function that adds all the event handlers to the html
document.
#+NAME: demo-handlers
#+BEGIN_SRC js
function handlers() { 
  document.getElementById("interval").oninput = function() { change_interval(); };
  document.getElementById("interval").onchange = function() { change_interval(); };
  document.getElementById("next").onclick = function() { start_sort(); };
  document.getElementById("reset").onclick = function() { reload(); };
  document.getElementById("pause").onclick = function() { pause(); };
}

#+END_SRC

* Function Randomize
+ Creates a randomised array of size 10
+ Places these random values inside DOM cards
+ Assign handlers
#+NAME: demo-randomise
#+BEGIN_SRC js
function randomise() { 
  var classToFill = document.getElementById("cards");
  for (var i = 0; i < selection_artefact.numOfCards; i++ ){
    selection_artefact.num[i] = Math.floor(Math.random() * 90 + 10);
    var temp = document.createElement("div");
    temp.className = "card";
    temp.innerHTML = selection_artefact.num[i];
    temp.style.fontStyle = "normal";
    temp.style.color = "white";
    classToFill.appendChild(temp);
  }
  document.getElementById("pause").disabled = true;
  document.getElementById("pause").style.backgroundColor = "grey";
}

#+END_SRC

* Function Change Interval
+ Supports changing time interval when the slider is changed
  in the webpage
+ Also, changes the background colour of Pause button if
  required
#+NAME: demo-slider
#+BEGIN_SRC js
function change_interval() {
  if(selection_artefact.prev == -1 && (selection_artefact.action == 1 || selection_artefact.action == -1)){
    if(selection_artefact.interval != 0) 
      clearInterval(selection_artefact.interval);
    
    if(document.getElementById("interval").value != 100)
    {
      if(selection_artefact.fn_name > "") 
        selection_artefact.interval = setInterval(next_step, 2600-document.getElementById("interval").value);
      document.getElementById("pause").style.backgroundColor = "#288ec8";
    }
    else
      document.getElementById("pause").style.backgroundColor = "grey";
  }
}

#+END_SRC

* Function compare
+ Compares two given numbers given as input to it
+ Highlights numbers that are under consideration by
  changing their background colour
+ Returns True if the swap has to be performed
+ Return False otherwise
#+NAME: demo-compare
#+BEGIN_SRC js
function compare(i, j) {
  selection_artefact.comparisons++;
  for(var n = selection_artefact.iterator1 ; n < selection_artefact.numOfCards ; n++ )
  {
    if(n == j ) 
      selection_artefact.card[n].style.backgroundColor = "#a4c652"; 
    else if(n == i )
      selection_artefact.card[n].style.backgroundColor = "black";
    else
      selection_artefact.card[n].style.backgroundColor = "#288ec8";
  }
  document.getElementById("ins").innerHTML = "<p>Comparisons: " + selection_artefact.comparisons + " | Swaps: " + selection_artefact.swaps + "</p><p>Comparing " + selection_artefact.card[selection_artefact.minimum].innerHTML + " and " + selection_artefact.card[j].innerHTML + "...</p>";
  if(eval(selection_artefact.card[j].innerHTML) < eval(selection_artefact.card[i].innerHTML))
  {
    document.getElementById("ins").innerHTML += "<p> Minimum updated to " + selection_artefact.card[j].innerHTML +"</p>"
    return true;
  }
  else
  {
    document.getElementById("ins").innerHTML += "<p>Minimum update not required</p>"
    return false;
  }
}

#+END_SRC

* Function Swap
Swaps the two elements whose indices are given as input
#+NAME: demo-swap
#+BEGIN_SRC js
function swap(i, j) {
  selection_artefact.swaps++;
  var temp;
  document.getElementById("ins").innerHTML = document.getElementById("ins").innerHTML + "<p>Swapping " + selection_artefact.card[i].innerHTML + " and " + selection_artefact.card[j].innerHTML + "</p>";
  temp = eval(selection_artefact.card[j].innerHTML);
  selection_artefact.card[j].innerHTML = eval(selection_artefact.card[i].innerHTML);
  selection_artefact.card[i].innerHTML = temp;
  selection_artefact.card[i].style.backgroundColor = "grey";
}

#+END_SRC

* Function Selection
+ Main driving function for this sorting algorithm
+ Updates the values of iterator1 and iterator2 and checks
  for termination conditions
#+NAME: demo-selectionsort
#+BEGIN_SRC js
function selection() {
  if(selection_artefact.iterator2 < selection_artefact.numOfCards-1)
    selection_artefact.iterator2++;
  else
  {
    if(selection_artefact.iterator1==selection_artefact.numOfCards-1)
    {
      if(document.getElementById("interval").value != 100){
        clearInterval(selection_artefact.interval);
        selection_artefact.interval = 0;
      }
      document.getElementById("ins").innerHTML = "<h3>The sort is complete - there were " + selection_artefact.comparisons + " comparisons and " + selection_artefact.swaps + " swaps.</h3>";
      document.getElementById("next").style.backgroundColor = "grey";
      document.getElementById("next").disabled = true;
      document.getElementById("pause").style.backgroundColor = "grey";
      document.getElementById("pause").disabled = true;
      selection_artefact.iterator2 = 0;
    }
    else
    {
      selection_artefact.finished = true;
      swap(selection_artefact.iterator1, selection_artefact.minimum);
      selection_artefact.iterator1++;
      selection_artefact.iterator2 = selection_artefact.iterator1+1;
      selection_artefact.minimum = selection_artefact.iterator1; 
    }
  }
};

#+END_SRC

* Function next step
+ Performs operations based on a given action
+ Highlights required elements
+ Begins a new iteration at the end of a new one (until
  termination)
#+NAME: demo-next-step
#+BEGIN_SRC js
function next_step() {
  if (selection_artefact.iterator1==selection_artefact.numOfCards-1) 
  {
    selection_artefact.card[selection_artefact.iterator1].style.backgroundColor = "grey";
    document.getElementById("ins").innerHTML = "<h3>The sort is complete - there were " + selection_artefact.comparisons + " comparisons and " + selection_artefact.swaps + " swaps.</h3>";
    document.getElementById("next").style.backgroundColor = "grey";
    document.getElementById("next").disabled = true;
    document.getElementById("pause").style.backgroundColor = "grey";
    document.getElementById("pause").disabled = true;
    pause();
    return;
  }       
  if(selection_artefact.action == 1)
  {
    if(compare(selection_artefact.minimum, selection_artefact.iterator2))
      selection_artefact.action = -1;
    else
      window[selection_artefact.fn_name]();
  }
  else
  {
    selection_artefact.action = 1;
    selection_artefact.minimum = selection_artefact.iterator2;
    selection_artefact.finished = false;
    window[selection_artefact.fn_name](); 
  }
}

#+END_SRC

* Function Pause
+ Called when Pause button is clicked
+ Makes the value of interval the minimum possible, so that
  the delay (2600-100) is maximum.
#+NAME: demo-pause
#+BEGIN_SRC js
function pause() {
  if(selection_artefact.prev == -1){
    selection_artefact.prev = document.getElementById("interval").value;
    if(selection_artefact.interval != 0) 
      clearInterval(selection_artefact.interval);
    document.getElementById("pause").value = "Play";
  }else{
    selection_artefact.prev = -1;
    if(selection_artefact.fn_name > "") 
      selection_artefact.interval = setInterval(next_step, 2600-document.getElementById("interval").value);
    document.getElementById("pause").value = "Pause";
  }
}

#+END_SRC

* Function Start sort
+ Called when Start is clicked on the webpage
+ Starts the sorting regime
+ Initializes variables that are going to be used
+ Calls the required function based on the sorting algorithm
+ Set the interval by calling change_interval
#+NAME: demo-start-sort
#+BEGIN_SRC js
function start_sort() {
  if(selection_artefact.interval != 0) { 
    clearInterval(selection_artefact.interval); 
    selection_artefact.interval = 0;
  }
  document.getElementById("comment-box-bigger").style.visibility = "visible";
  selection_artefact.card = document.querySelectorAll('.card');
  selection_artefact.action = 1;
  selection_artefact.finished = true;
  selection_artefact.comparisons = 0;
  selection_artefact.swaps = 0;
  selection_artefact.fn_name = "selection";
  
  selection_artefact.iterator1 = 0;
  selection_artefact.iterator2 = 1;
  selection_artefact.operation = "Swap";
  next_step();

  document.getElementById("next").onclick = function() { next_step(); };
  document.getElementById("next").value = "Next";
  document.getElementById("pause").disabled = false;
  document.getElementById("pause").style.backgroundColor = "#288ec8";

  if(document.getElementById("interval").value == 100)
  {
    document.getElementById("next").disabled = false;
  }
  else
  {
    document.getElementById("pause").style.visibility = "visible";
    if(selection_artefact.interval == 0)
      selection_artefact.interval = setInterval(next_step, 2600-document.getElementById("interval").value);
    else
    {
      clearInterval(selection_artefact.interval);
      selection_artefact.interval = 0;
    }
  }
}

#+END_SRC

* Function reload
Used to reload the artefact when Reset button is clicked
#+NAME: demo-reload
#+BEGIN_SRC js
function reload(){
  location.reload(true);
}

#+END_SRC

* Tangle
#+BEGIN_SRC js :tangle selection_demo.js :eval no :noweb yes
<<demo-universal-class>>
<<main-functions>>
<<demo-handlers>>
<<demo-randomise>>
<<demo-slider>>
<<demo-compare>>
<<demo-swap>>
<<demo-selectionsort>>
<<demo-next-step>>
<<demo-pause>>
<<demo-start-sort>>
<<demo-reload>>
#+END_SRC
