#+TITLE: Demonstration artefact for Selection Sort
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =selection sort demo= interactive
  artefact(HTML).

* Features of the artefact
  + Artefact provides demo for simple selection sort
  + User can see a randomly generated array in the
    beginning.
  + User can click the =Reset= button to generate a new
    array at any time.
  + User shall click the =Start= button to start the demo.
  + User can slide a =Slider= to adjust speed of the demo as
    per his/her convenience.
  + User can pause the demo by clicking on the =Pause=
    button.
  + User can pause and then click on =Next= manually to view
    the demo at his/her own speed.

* HTML Framework of Selection Sort
** Head Section Elements
Title, meta tags and all the links related to CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Selection Sort</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/selection_css.css">
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>Click on the <b>Start</b> button to start the
      demo.</li>
      <li>Move the slider to adjust the speed of the
      demo.</li>
      <li>Click on the <b>Pause</b> button if you want to stop
      and manually click the <b>Next</b> button to have a step
      by step realization of the process.</li>
      <li>Click on the <b>Reset</b> button to start all over
      with a new set of random numbers!</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Cards Holder
This is a cards holder, which contains 10 =<div class=card>=
elements as, we are keeping the size(10) of an array
static. In each of this div elements array elements are
generated as cards dynamically.
#+NAME: cards-holder
#+BEGIN_SRC html
<div id="cards"></div>

#+END_SRC

*** Legend
This is a legend, that can referred to when observing.
#+NAME: legend
#+BEGIN_SRC html
<ul class="legend">
  <li><span class="grey"></span> Sorted Elements</li>
  <li><span class="green"></span> Element being iterated</li>
  <li><span class="black"></span> Minimum element in current iteration</li>
</ul>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed, based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div class="comment-box" id="comment-box-bigger">
  <b>Observations</b>
  <p id="ins"></p>
</div>

#+END_SRC

*** Slider Container
This is a slider to which =change_interval= method is
appended to control the demonstration speed of an artefact.
#+NAME: slider-container
#+BEGIN_SRC html
<div class="slidecontainer">
  <p>
    Min. Speed
    <input class="slider" type="range" min="100" max="2500" id="interval" value="1500" >
    Max. Speed
  </p>
</div>

#+END_SRC

*** Buttons Wrapper
This is a div wrapped with buttons used for
performing/demonstrating an artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <input class="button-input" type="button" value="Start" id="next">
  <input class="button-input" type="button" value="Reset" id="reset">
  <input class="button-input" type="button" value="Pause" id="pause">
</div>

#+END_SRC

** Javascript
This comprises of all the Javascript needed to run or add
behavior to the artefact.
#+NAME: js-elems
#+BEGIN_SRC html
<script src="../js/selection_demo.js"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle selection_demo.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>>
    <div id="wrapper">
      <<cards-holder>>
      <div id="legend-comment-wrapper">
        <<legend>>
        <<comments-box>>
      </div>
      <<slider-container>>
      <<buttons-wrapper>>
    </div>
    <<js-elems>>
  </body>
</html>
#+END_SRC
